USAGE = """*** Wordle Helper ***
Enter white-space separated letter, color, optional position
Format: Letter Color [Position]
Letter: a-z, A-Z
Color: g, y, b, G, Y, B
Position: 1-5"""
PROMPT = 'Letter Color [Position] (q to quit):'
QUIT = 'Quitting'


def get_input():
    retry = True
    while retry:
        print(PROMPT)
        inp = input().split()
        letter = inp[0].lower()
        if not (len(letter) == 1 and letter.isalpha() and letter.islower()):
            retry = True
            continue
        if letter == 'q':
            return [False, False, False, False]
        color = inp[1].lower()
        if not (
            len(color) == 1 and (
                color == 'g' or color == 'y' or color == 'b')):
            retry = True
            continue
        position = -1
        if (color == 'g' or color == 'y'):
            if not (len(inp) == 3):
                retry = True
                continue
            position = int(inp[2]) - 1
        if (color == 'g' or color == 'y') and not (
                position >= 0 and position <= 4):
            retry = True
            continue
        retry = False
    return [letter, color, position, True]


if __name__ == "__main__":
    dictionary = []
    with open('sgb-words.txt') as f:
        dictionary = f.read().splitlines()
    list = dictionary.copy()

    print(USAGE)
    [letter, color, position, running] = get_input()
    while running:
        new_list = []
        for word in list:
            if color == 'g' and word[position] == letter:
                new_list.append(word)
            if color == 'y' and letter in word and word[position] != letter:
                new_list.append(word)
            if color == 'b' and not letter in word:
                new_list.append(word)
        for w in new_list:
            print(w)
        print(f'{len(new_list)} possible words')
        list = new_list
        [letter, color, position, running] = get_input()
    print(QUIT)
