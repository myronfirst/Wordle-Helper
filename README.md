# Wordle Helper

Helper tool for 5-letter-word games like Worlde.

Imports a dictionary from `sgb-words.txt` and eliminates words based on user input.

Run as python script.
